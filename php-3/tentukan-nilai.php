<!-- tangkas -->
<?php
function tentukan_nilai($number)
{
    if ($number >= 77) {
        echo "Sangat Baik ";
    } else if ($number >= 75 && $number <= 77) {
        echo "Baik ";
    } else if ($number >= 68 && $number <= 75) {
        echo "Cukup ";
    } else {
        echo "Kurang ";
    }
    return $number . "<br>";
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
