<?php
function ubah_huruf($string)
{
    $huruf = strtolower($string);
    $abjad = [];
    foreach (range("a", "z") as $a) {
        array_push($abjad, $a);
    }
    $output = [];

    for ($i = 0; $i < strlen($huruf); $i++) {
        for ($j = 0; $j < count($abjad); $j++) {
            if ($huruf[$i] == $abjad[$j]) {
                array_push($output, $abjad[$j + 1]);
            }
        }
    }
    $output = implode("", $output);
    return $output . "<br>";
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo "<br>";
echo ubah_huruf('developer'); // efwfmpqfs
echo "<br>";
echo ubah_huruf('laravel'); // mbsbwfm
echo "<br>";
echo ubah_huruf('keren'); // lfsfo
echo "<br>";
echo ubah_huruf('semangat'); // tfnbohbu
echo "<br>";
